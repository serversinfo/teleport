//Teleport: by Spazman0

#pragma newdecls required
#include <sdktools>
#include <adminmenu>
/*****************************************************************
			G L O B A L   V A R S
*****************************************************************/
float g_fvPos[3];
TopMenu hTopMenu = null;


/*****************************************************************
			F O R W A R D   P U B L I C S
*****************************************************************/
public void OnPluginStart()
{
	RegAdminCmd("sm_tele", Command_Tele, ADMFLAG_SLAY, "sm_tele <#userid|name> - Teleports player to where admin is looking");
	OnMapStart();
}
public void OnMapStart()
{
	TopMenu topmenu;
	if (LibraryExists("adminmenu") && ((topmenu = GetAdminTopMenu()) != null)) {
		if (topmenu == hTopMenu)			// Block us from being called twice
			return;
		hTopMenu = topmenu;
		TopMenuObject player_commands = hTopMenu.FindCategory(ADMINMENU_PLAYERCOMMANDS);		// Find the "Player Commands" category
		if (player_commands != INVALID_TOPMENUOBJECT)
			hTopMenu.AddItem("sm_tele", AdminMenu_Tele, player_commands, "sm_tele", ADMFLAG_SLAY);
	}
}

/****************************************************************
			C A L L B A C K   F U N C T I O N S
****************************************************************/
public Action Command_Tele(int client, int args)
{
	char sTarget[32], sTarget_name[MAX_NAME_LENGTH];
	int iTarget_list[MAXPLAYERS], iTarget_count;
	bool bTn_is_ml;
	
	//validate args
	if (args < 1) {
		ReplyToCommand(client, "[SM] Usage: sm_tele <#userid|name>");
		return Plugin_Handled;
	}
	
	if( !client ) {
		ReplyToCommand(client, "[SM] Cannot teleport from rcon");
		return Plugin_Handled;	
	}
	
	//get argument
	GetCmdArg(1, sTarget, sizeof(sTarget));
	
	//get sTarget(s)
	if ((iTarget_count = ProcessTargetString(	sTarget,
												client,
												iTarget_list,
												MAXPLAYERS,
												COMMAND_FILTER_ALIVE,
												sTarget_name,
												sizeof(sTarget_name),
												bTn_is_ml)) <= 0) {
		ReplyToTargetError(client, iTarget_count);
		return Plugin_Handled;
	}
	
	if(!SetTeleportEndPoint(client))
		return Plugin_Handled;
	
	for (int i=0; i<iTarget_count; i++) {
		TeleportEntity(iTarget_list[i], g_fvPos, NULL_VECTOR, NULL_VECTOR);
		LogAction(client, iTarget_list[i], "\"%L\" teleported \"%L\"", client, iTarget_list[i]);
	}
	
	ShowActivity2(client, "[SM] ","%N телепортировал %s", client,  sTarget_name);
	
	return Plugin_Handled;	
}

public bool TraceEntityFilterPlayer(int entity, int contentsMask)
{
	return entity > GetMaxClients() || !entity;
}

/*****************************************************************
			P L U G I N   F U N C T I O N S
*****************************************************************/
bool SetTeleportEndPoint(int client)
{
	float fvOrigin[3];
	float fvAngles[3];
	
	GetClientEyePosition(client, fvOrigin);
	GetClientEyeAngles(client, fvAngles);
	
	//get endpoint for teleport
	Handle trace = TR_TraceRayFilterEx(fvOrigin, fvAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
	
	if(TR_DidHit(trace)) {   
		float fvBuffer[3];
		float fvStart[3];
		TR_GetEndPosition(fvStart, trace);
		GetVectorDistance(fvOrigin, fvStart, false);
		float fDistance = -35.0;
		GetAngleVectors(fvAngles, fvBuffer, NULL_VECTOR, NULL_VECTOR);
		g_fvPos[0] = fvStart[0] + (fvBuffer[0]*fDistance);
		g_fvPos[1] = fvStart[1] + (fvBuffer[1]*fDistance);
		g_fvPos[2] = fvStart[2] + (fvBuffer[2]*fDistance);
		CloseHandle(trace);
		return true;
	} else {
		PrintToChat(client, "[SM] Не удалось телепортировать игрока");
		CloseHandle(trace);
		return false;
	}
}

/*****************************************************************
			A D M I N   M E N U   F U N C T I O N S
*****************************************************************/

public void AdminMenu_Tele(TopMenu topmenu, TopMenuAction action, TopMenuObject object_id, int client, char[] buffer, int maxlength)
{
	if (action == TopMenuAction_DisplayOption)
		Format(buffer, maxlength, "Телепортировать игрока");
	else if (action == TopMenuAction_SelectOption)
		DisplayTeleMenu(client);
}

void DisplayTeleMenu(int client)
{
	Menu menu = new Menu(MenuHandler_Tele);
	
	char sTitle[100];
	Format(sTitle, sizeof(sTitle), "Телепортировать игрока:");
	menu.SetTitle(sTitle);
	menu.ExitBackButton = true;
	
	AddTargetsToMenu(menu, client, true, true);
	
	menu.Display(client, MENU_TIME_FOREVER);
}


public int MenuHandler_Tele(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
		delete menu;
	else if (action == MenuAction_Cancel) {
		if (param2 == MenuCancel_ExitBack && hTopMenu != null)
			DisplayTopMenu(hTopMenu, param1, TopMenuPosition_LastCategory);
	} else if (action == MenuAction_Select) {
		char sInfo[32];
		int userid, iTarget;
		
		menu.GetItem(param2, sInfo, sizeof(sInfo));
		userid = StringToInt(sInfo);

		if ((iTarget = GetClientOfUserId(userid)) == 0)
			PrintToChat(param1, "[SM] Игрок больше не доступен");
		else if (!CanUserTarget(param1, iTarget))
			PrintToChat(param1, "[SM] Нет цели");
		else {
			char sName[32];
			GetClientName(iTarget, sName, sizeof(sName));
			
			if(IsClientInGame(iTarget) && IsPlayerAlive(iTarget) && SetTeleportEndPoint(param1)) {
				TeleportEntity(iTarget, g_fvPos, NULL_VECTOR, NULL_VECTOR);
				LogAction(param1, iTarget, "\"%L\" teleported \"%L\"", param1, iTarget);
				ShowActivity2(param1, "[SM] ","%N телепортировал %s", param1, sName);
			}
		}
		
		/* Re-draw the menu if they're still valid */
		if (IsClientInGame(param1) && !IsClientInKickQueue(param1))
			DisplayTeleMenu(param1);
	}
}